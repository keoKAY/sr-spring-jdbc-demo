package com.kshrd.srdemospringjdbc.controller;


import com.kshrd.srdemospringjdbc.model.Student;
import com.kshrd.srdemospringjdbc.repository.StudentRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StudentController {


    // field injection
    // setter injection
    // construction
    @Autowired
    StudentRepository studentRepository;

    @GetMapping()
    public String index(){

        System.out.println("Here is value of student");
        //studentRepository.getAllStudentsOldWay().stream().forEach(System.out::println);
      //  studentRepository.getAllStudents().stream().forEach(System.out::println);


        //
        Student insertStudent = new Student(8,"Rina Update","M","Hi ! I like mathematic ",2);

//       int resultUpdate= studentRepository.updateStudent(insertStudent);
//       System.out.println("Here is reResultInsert:"+resultUpdate);

        int resultDelete= studentRepository.deleteStudent(8);
       System.out.println("Here is reResultDelete:"+resultDelete);

      // System.out.println("Find student 8:"+studentRepository.findById(8));



        return "index";
    }


}

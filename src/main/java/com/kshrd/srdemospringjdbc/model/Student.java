package com.kshrd.srdemospringjdbc.model;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class Student {
    private int id;
    private String username;
    private String gender;
    private String bio;
    private  int course_id;


}

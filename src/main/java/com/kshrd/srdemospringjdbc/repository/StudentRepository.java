package com.kshrd.srdemospringjdbc.repository;

import com.kshrd.srdemospringjdbc.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Student> getAllStudentsOldWay(){

        List<Student> studentList = new ArrayList<>();
        String username = "postgres";
        String password = "password";
        String url="jdbc:postgresql://localhost:5432/testdb";


        String query = "select * from students";
        try(
                Connection connection = DriverManager.getConnection(url, username, password);
                Statement statement = connection.createStatement();
                ResultSet resultSet  = statement.executeQuery(query);

                ){
            // mapp value from resultset
            while(resultSet.next()){

                studentList.add(new Student(
                        resultSet.getInt("id"),
                        resultSet.getString("username"),
                        resultSet.getString("gender"),
                        resultSet.getString("bio"),
                        resultSet.getInt("course_id")
                ));
            }

        }catch (SQLException ex){

            System.out.println("SQL Exception : "+ex.getMessage());
        }

        return studentList;

    }

    public  List<Student> getAllStudents (){
       // return jdbcTemplate.query("select * from students",new BeanPropertyRowMapper<>(Student.class));

//        return  jdbcTemplate.query("select * from students", new RowMapper<Student>() {
//            @Override
//            public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
//                return new Student(
//                        rs.getInt("id"),
//                        rs.getString("username"),
//                        rs.getString("gender"),
//                        rs.getString("bio"),
//                        rs.getInt("course_id")
//                );
//            }
//        });

    //    return jdbcTemplate.query("select * from students",(rs,rowNum)->this.rowMapping(rs,rowNum));
  return  jdbcTemplate.query("select * from students",this::rowMapping);

    }

    // find by id
    public Student findById(int id){
        return jdbcTemplate.queryForObject("select * from students where id =?",new BeanPropertyRowMapper<>(Student.class),new Object[]{id});
    }
    //insert

    public int insertStudent (Student student){

        return  jdbcTemplate.update("insert into students(id,username,gender,bio,course_id) values (?,?,?,?,?)",
                student.getId(),
                student.getUsername(),
                student.getGender(),
                student.getBio(),
                student.getCourse_id());
    }

    // update
    public int updateStudent(Student student){
        return jdbcTemplate.update("update students set username=?,gender=?,  bio=?,course_id=? where id=?",
                student.getUsername(),
                student.getGender(),
                student.getBio(),
                student.getCourse_id(),
                student.getId()
                );
    }

    // delete
    public int deleteStudent(int id){
        return jdbcTemplate.update("delete from students where id=?",id);
    }


    public Student rowMapping(ResultSet rs, int numRow) throws SQLException {
        return new Student(
                        rs.getInt("id"),
                        rs.getString("username"),
                        rs.getString("gender"),
                        rs.getString("bio"),
                        rs.getInt("course_id")
                );
    }
}

package com.kshrd.srdemospringjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrDemoSpringJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrDemoSpringJdbcApplication.class, args);
    }

}
